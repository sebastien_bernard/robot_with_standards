*** Settings ***
Documentation       This is a gologic scraper for demo purpose :)

# 1) Ajouter un mauvais chemin ici
Resource            ../common/ressource.robot

Suite Teardown      Close All Browsers

*** Test Cases ***
Obtenir la description de la formation pipeline-as-code
    [Documentation]    Organize and plan product development
    Ouvrir le site web de GoLogic
    Naviguer a la formation pipeline-as-code
    Remplir le formulaire

*** Keywords ***
Ouvrir le site web de GoLogic
    Open Browser    ${gologic_url}    ${BROWSER}
    Wait Until Element Is Visible    //i[@class="eicon-close"]
    Click Element    //i[@class="eicon-close"]

Naviguer a la formation pipeline-as-code
    Wait Until Element Is Visible    //a[contains(text(),'Services')]
    Click Element    //a[contains(text(),'Services')]
    Wait Until Element Is Visible    //a[contains(text(),'Ateliers DevOps')]
    Click Element    //a[contains(text(),'Ateliers DevOps')]

Remplir le formulaire
    clear element text    //input[@name="your-name"]
    input text    //input[@name="your-name"]    mr.robot
    clear element text    //textarea[@name="your-message"]
    input text    //textarea[@name="your-message"]    Wow! J'en prends 4!
